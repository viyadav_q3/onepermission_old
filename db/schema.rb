# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160812071802) do

  create_table "accesses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "status",             limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "role_id"
    t.integer  "role_permission_id"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "activities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "content"
    t.string   "actionable_type"
    t.integer  "actionable_id"
    t.integer  "status",          limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["actionable_type", "actionable_id"], name: "index_activities_on_actionable_type_and_actionable_id", using: :btree
  end

  create_table "companies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "integration_accesses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "status",                    limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "company_id"
    t.integer  "integration_id"
    t.integer  "integration_permission_id"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  create_table "integration_permissions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.string   "identifier"
    t.integer  "status",      limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "integration_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.string   "identifier"
    t.integer  "status",      limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "integrations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "secret_key"
    t.string   "client_id"
    t.string   "access_token"
    t.integer  "status",              limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "integration_type_id"
    t.integer  "company_id"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  create_table "integrations_users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "integration_id"
    t.index ["integration_id"], name: "index_integrations_users_on_integration_id", using: :btree
    t.index ["user_id"], name: "index_integrations_users_on_user_id", using: :btree
  end

  create_table "orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float    "amount",                limit: 24
    t.integer  "mode_of_payment"
    t.string   "description"
    t.string   "transaction_id"
    t.integer  "payment_status",        limit: 1,  default: 0
    t.integer  "status",                limit: 1,  default: 0
    t.datetime "deleted_at"
    t.integer  "subscription_plans_id"
    t.integer  "subscription_id"
    t.integer  "companies_id"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "role_permissions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "controller"
    t.string   "action"
    t.string   "agroup"
    t.integer  "status",      limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.integer  "status",      limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "subscription_plans", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.float    "amount",          limit: 24
    t.integer  "max_user"
    t.integer  "max_integration"
    t.integer  "duration_days"
    t.integer  "status",          limit: 1,  default: 0
    t.datetime "deleted_at"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "subscriptions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "renew_count"
    t.datetime "start_at"
    t.datetime "expiry_at"
    t.integer  "status",               limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "subscription_plan_id"
    t.integer  "company_id"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "user_profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "title"
    t.bigint   "phone_number"
    t.integer  "status",       limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "user_id"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "username"
    t.string   "email",                            default: "",    null: false
    t.string   "encrypted_password",               default: "",    null: false
    t.integer  "status",                 limit: 1, default: 0
    t.datetime "deleted_at"
    t.integer  "role_id"
    t.integer  "company_id"
    t.boolean  "is_admin",                         default: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.integer  "deleted_by"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end

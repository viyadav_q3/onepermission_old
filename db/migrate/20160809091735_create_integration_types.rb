class CreateIntegrationTypes < ActiveRecord::Migration
  def change
    create_table :integration_types do |t|
      t.string :title
      t.string :identifier
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

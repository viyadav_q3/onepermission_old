class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :title, unique: true
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

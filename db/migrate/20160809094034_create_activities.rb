class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :content
      t.references :actionable, polymorphic: true, index: true
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

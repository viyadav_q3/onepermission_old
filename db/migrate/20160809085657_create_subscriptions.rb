class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.integer :renew_count
      t.datetime :start_at
      t.datetime :expiry_at
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :subscription_plan_id
      t.integer :company_id
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

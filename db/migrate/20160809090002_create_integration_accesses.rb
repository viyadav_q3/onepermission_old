class CreateIntegrationAccesses < ActiveRecord::Migration
  def change
    create_table :integration_accesses do |t|
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :company_id
      t.integer :integration_id
      t.integer :integration_permission_id
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

class CreateAccesses < ActiveRecord::Migration
  def change
    create_table :accesses do |t|
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :role_id
      t.integer :role_permission_id
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

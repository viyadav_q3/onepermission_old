class CreateIntegrations < ActiveRecord::Migration
  def change
    create_table :integrations do |t|
      t.string :secret_key, unique: true
      t.string :client_id, unique: true
      t.string :access_token, unique: true
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :integration_type_id
      t.integer :company_id
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

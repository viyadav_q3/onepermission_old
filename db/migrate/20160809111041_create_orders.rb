class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.float :amount
      t.integer :mode_of_payment
      t.string :description
      t.string :transaction_id
      t.integer :payment_status, default: 0, limit: 1
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.references :subscription_plans
      t.references :subscription
      t.references :companies
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by
      t.timestamps null: false
    end
  end
end

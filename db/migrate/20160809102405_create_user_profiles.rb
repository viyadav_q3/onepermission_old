class CreateUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :title
      t.integer :phone_number, limit: 8
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :user_id
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

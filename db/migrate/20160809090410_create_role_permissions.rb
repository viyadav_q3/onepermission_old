class CreateRolePermissions < ActiveRecord::Migration
  def change
    create_table :role_permissions do |t|
      t.string :controller
      t.string :action
      t.string :agroup
      t.integer :status, default: 0, limit: 1
      t.datetime :deleted_at
      t.integer :created_by
      t.integer :modified_by
      t.integer :deleted_by

      t.timestamps null: false
    end
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Role::TITLES.each do |role|
  role = Role.find_or_create_by(title: role, status: 0)
end

User.transaction do 
	user = User.find_or_initialize_by(email: Rails.application.secrets.admin_email, status: 0, username: Rails.application.secrets.admin_username, role: Role.first)
	user.password = Rails.application.secrets.admin_password
	if user.new_record? && user.valid?
		user.skip_confirmation!
		user.build_user_profile(first_name: Rails.application.secrets.admin_first_name, last_name: Rails.application.secrets.admin_last_name, company: Rails.application.secrets.admin_company, title: Rails.application.secrets.admin_title, phone_number: Rails.application.secrets.admin_phone, status: 0)
		user.save
	else
		puts user.errors.full_messages
	end
end

IntegrationPermission::INT_PERMISSIONS.each do |ip|
	IntegrationPermission.find_or_create_by(title: ip, status: 0)
end

IntegrationType.find_or_create_by(title: "google analytics", status: 0)
class IntegrationPermission < ActiveRecord::Base
	has_many :integration_accesses

	INT_PERMISSIONS = ["read", "write", "execute", "delete"]
end

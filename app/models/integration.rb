class Integration < ActiveRecord::Base
	has_many :integration_accesses
    has_and_belongs_to_many :users

	belongs_to :company
	belongs_to :integration_type
end

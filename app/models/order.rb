class Order < ActiveRecord::Base
	belongs_to :company
	belongs_to :subscription_plan
	belongs_to :subscription
end

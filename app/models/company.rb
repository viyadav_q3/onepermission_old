class Company < ApplicationRecord
	has_many :subscriptions
	has_many :integration_accesses
	has_many :integrations
	has_many :activities,  as: :actionable
	has_many :orders
	has_many :users
end

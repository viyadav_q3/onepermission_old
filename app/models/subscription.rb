class Subscription < ActiveRecord::Base
	has_many :orders
	belongs_to :subscription_plan
	belongs_to :company
end

# rails 5 added this class just like application controller
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end

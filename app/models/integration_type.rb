class IntegrationType < ActiveRecord::Base
	has_many :integrations

	before_save :create_identifier, if: :title_changed?


	private
	def create_identifier
		#using rails 5 feature
		salt  = SecureRandom.random_bytes(64)
		key   = ActiveSupport::KeyGenerator.new('password').generate_key(salt) # => "\x89\xE0\x156\xAC..."
		crypt = ActiveSupport::MessageEncryptor.new(key)                       # => #<ActiveSupport::MessageEncryptor ...>
		self.identifier = crypt.encrypt_and_sign(self.title)              # => "NlFBTTMwOUV5UlA1QlNEN2xkY2d6eThYWWh..."
		
		# ---> for decryption ---<
		# crypt.decrypt_and_verify(encrypted_data)
		# ---> for decryption ---<
	end
end

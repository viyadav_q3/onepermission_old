class IntegrationAccess < ActiveRecord::Base
	belongs_to :integration_permission
	belongs_to :company
	belongs_to :integration
end
